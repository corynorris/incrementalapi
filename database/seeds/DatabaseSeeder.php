<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Laracasts\TestDummy\Factory as TestDummy;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

        TestDummy::times(10)->create('App\Lesson');
        TestDummy::times(30)->create('App\Tag');

        TestDummy::times(30)->create('App\Lesson_Tag');


    }

}
