<?php
 /**  
 * Author: Cory Norris 
 * Created At: 08/06/15, 10:28 AM
 */


namespace app\Norris\Transformers;

abstract class Transformer {

    /**
     * Transform a collection of items
     *
     * @param $items
     * @return array
     */
    public function transformCollection($items)
    {
        return array_map([$this, 'transform'], $items);
    }


    public abstract function transform($item);

}