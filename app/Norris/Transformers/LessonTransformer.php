<?php
 /**  
 * Author: Cory Norris 
 * Created At: 08/06/15, 10:21 AM
 */


namespace app\Norris\Transformers;


use App\Norris\Transformers\Transformer;

class LessonTransformer extends Transformer{


    public function transform($lesson) {
        return [
            'title' => $lesson['title'],
            'body' => $lesson['body'],
            'active' => (boolean)$lesson['active'],
            'id' =>$lesson['id']
        ];
    }
}