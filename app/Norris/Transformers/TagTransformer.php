<?php
 /**  
 * Author: Cory Norris 
 * Created At: 10/06/15, 2:01 AM
 */


namespace app\Norris\Transformers;

use App\Norris\Transformers\Transformer;


class TagTransformer extends Transformer{

    public function transform($tag) {
        return [
            'name' => $tag['name']
        ];
    }
}