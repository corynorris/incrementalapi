<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson_Tag extends Model {

    protected $table = 'lesson_tag';

    public $timestamps = false;

}
