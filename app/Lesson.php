<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model {

    protected $table = 'lesson';

	protected $fillable = ['title', 'body', 'active'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags() {
        return $this->belongsToMany('App\Tag');
    }
}
