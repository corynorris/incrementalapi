<?php
/**
 * Author: Cory Norris
 * Created At: 08/06/15, 8:12 PM
 */


namespace App\Http\Controllers;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Response as IlluminateResponse;


/**
 * Class ApiController
 * @package app\Http\Controllers
 */
class ApiController extends Controller {


    /**
     * @var int
     */
    protected $statusCode = 200;

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @param string $message
     * @return mixed
     */
    public function respondNotFound($message = 'Not Found')
    {

        return $this->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND)->respondWithError($message);
    }

    /**
     * @param $data
     * @param array $headers
     * @return mixed
     */
    public function respond($data, $headers = [])
    {
        return Response::json($data, $this->getStatusCode(), $headers);
    }

    /**
     * @param $lessons
     * @param $data
     * @return mixed
     */
    public function respondWithPagination(Paginator $lessons, $data)
    {
        array_merge($data, [
            $data,
            'paginator' => [
                'total_count'  => $lessons->total(),
                'total_pages'  => $lessons->lastPage(),
                'current_page' => $lessons->currentPage(),
                'limit'        => $lessons->perPage(),
                'next_page'    => $lessons->nextPageUrl(),
            ]
        ]);

        return $this->respond($data);
    }

    /**
     * @param $message
     * @return mixed
     */
    public function respondWithError($message)
    {
        return $this->respond([
            'error' => [
                'message'     => $message,
                'status_code' => $this->getStatusCode()
            ]
        ]);
    }

    /**
     * @param $message
     * @return mixed
     */
    public function respondCreated($message)
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_CREATED)->respond([
            'message' => $message
        ]);
    }

    public function respondUnprocessible($message)
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY)
            ->respondWithError($message);

    }
}