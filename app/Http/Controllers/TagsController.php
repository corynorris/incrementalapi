<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lesson;
use App\Norris\Transformers\TagTransformer;
use App\Tag;
use Illuminate\Http\Request;

class TagsController extends ApiController {


    protected $tagTransformer;

    public function __construct(TagTransformer $tagTransformer)
    {
        $this->tagTransformer = $tagTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @param null $lessonId
     * @return Response
     */
	public function index($lessonId = null)
	{
        $tags = $this->getTags($lessonId);

		return $this->respond([
            'data' => $this->tagTransformer->transformCollection($tags->all())
        ]);
	}

	/**
     * @param $lessonId
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public function getTags($lessonId)
    {
        return $lessonId ?
            Lesson::findOrFail($lessonId)->tags :
            Tag::all();

    }

}
