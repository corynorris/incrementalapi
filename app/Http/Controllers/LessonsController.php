<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Norris\Transformers\LessonTransformer;

class LessonsController extends ApiController {

    /**
     * @var Norris\Transformers\LessonTransformer LessonTransformer
     */
    protected $lessonTransformer;

    public function __construct(LessonTransformer $lessonTransformer)
    {
        $this->lessonTransformer = $lessonTransformer;
        $this->middleware('auth.basic', ['only' => 'store']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $lessons = Lesson::paginate(5);
        //$lessons->setPath('');

        if ( ! $lessons)
        {
            return $this->respondNotFound('Lesson does not exist.');
        }

        return $this->respondWithPagination($lessons, [
                'data' => $this->lessonTransformer->transformCollection($lessons->all()),
            ]
        );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ( ! Input::get('title') or ! Input::get('body'))
        {
            // How to respond to a failed validation?
            //400 bad request
            //403 forbidden
            //422 unprocessible entity
            return $this->respondUnprocessible('Parameters failed validation for a lesson');
        }

        Lesson::create(Input::all());

        return $this->respondCreated('Lesson successfully created.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $lesson = Lesson::find($id);

        if ( ! $lesson)
        {
            return $this->respondNotFound('Lesson does not exist.');
        }

        return $this->respond([
            'data' => $this->lessonTransformer->transform($lesson)
        ]);
    }


}
