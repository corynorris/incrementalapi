<?php
/**
 * Author: Cory Norris
 * Created At: 07/06/15, 12:47 PM
 */

$factory('App\Lesson', [
        'title'  => $faker->sentence(3),
        'body'   => $faker->paragraph(4),
        'active' => $faker->boolean()
    ]
);

$factory('App\Tag', [
        'name' => $faker->word()
    ]
);

$factory('App\Lesson_Tag', [
        'lesson_id' => $faker->randomElement(range(1, 10)), //'factory:App\Lesson',
        'tag_id'    => $faker->randomElement(range(1, 30)) //'factory:App\Tag'
    ]
);