<?php

use App\User;
use Tests\ApiTester;

/**
 * Class LessonTest
 */
class LessonTest extends ApiTester {


    /** @test */
    public function it_fetches_lessons()
    {
        // Arrange
        $this->times(5)->make('App\Lesson');

        // Act
        $this->getJson('api/v1/lessons');

        // Assert
        $this->assertResponseOk();

    }

    /** @test */
    public function it_fetches_a_single_lesson()
    {
        // Arrange
        $this->make('App\Lesson');

        // Act
        $lesson = $this->getJson('api/v1/lessons/1')->data;

        // Assert
        $this->assertResponseOk();
        $this->assertObjectHasAttributes($lesson, 'body', 'active');

    }

    /** @test */
    public function it_404s_if_a_lesson_is_not_found()
    {

        $this->getJson('api/v1/lessons/x');
        $this->assertResponseStatus(404);
    }

    /** @test */
    public function it_creates_a_new_lesson_given_post_request()
    {
        $user = new User(array('name' => 'John'));
        $this->be($user); //You are now authenticated

        $this->getJson('api/v1/lessons', 'POST', $this->getStub());

        $this->assertResponseStatus(201);
    }

    /** @test */
    public function it_throws_a_422_if_a_new_lesson_request_fails_validation()
    {
        $this->authenticate();

        $this->getJson('api/v1/lessons', 'POST');
        $this->assertResponseStatus(422);
    }

    /**
     * @return array
     */
    public function getStub() {
        return [
            'title' => $this->fake->sentence,
            'body'  => $this->fake->paragraph,
            'active'=> $this->fake->boolean
        ];
    }


}
