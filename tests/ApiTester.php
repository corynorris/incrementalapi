<?php
 /**  
 * Author: Cory Norris 
 * Created At: 11/06/15, 12:39 AM
 */

namespace tests;

use App\User;
use \Faker\Factory as Faker;
use Illuminate\Support\Facades\Artisan;
use TestCase;



/**
 * Class ApiTester
 * @package tests
 */
abstract class ApiTester extends TestCase {

    /**
     * @var \Faker\Generator
     */
    protected $fake;

    /**
     * @var int
     */
    protected $times = 1;

    /**
     *
     */
    function __construct()
    {

        $this->fake = Faker::create();
    }

    /**
     * Initialize
     */
    public function setUp()
    {
        parent::setUp();

        Artisan::call('migrate');
        //$this->app['artisan']->call('migrate');

    }

    protected function authenticate()
    {
        $user = new User(array('name' => 'John'));
        $this->be($user); //You are now authenticated
    }

    /**
     * Create $count times the object for testing
     *
     * @param $count
     * @return $this
     */
    protected function times($count) {

        $this->times = $count;

        return $this;

    }


    /**
     * Make a new record for the DB
     *
     * @param $type
     * @param array $fields
     */
    protected function make($type, array $fields = [])
    {
        while ($this->times--) {
            $stub = array_merge($this->getStub(), $fields);
            $type::create($stub);
        }
    }

    /**
     *  Create methods for make
     */
    protected function getStub() {
        throw new \BadMethodCallException('Create your own getStub method to create your fields');
    }

    /**
     * Get JSON output from API
     *
     * @param $uri
     * @param string $method
     * @param array $parameters
     * @return string
     */
    protected function getJson($uri, $method = 'GET', $parameters = [])
    {
        return json_decode($this->call($method, $uri, $parameters)->getContent());
    }

    /**
     * Guarantee an object has certain attributes
     * example assertObjectHasAttribute($obj, 'attr1', 'attr2', 'etc');
     */
    protected function assertObjectHasAttributes()
    {
        $args = func_get_args();
        $object = array_shift($args);


        foreach ($args as $attribute)
        {
           $this->assertObjectHasAttribute($attribute, $object);
        }
    }

}